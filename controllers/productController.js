const Product = require('../models/Product');
const User = require('../models/User');

//Create a new Product
module.exports.addProduct = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return "Not an Admin"
        } else {
            let newProduct = new Product({
                img: reqBody.img,
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price,
                stock: reqBody.stock
            })
        
            return newProduct.save().then((product, error) => {
                if(error) {
                    return false
                } else {
                    return true
                }
            })
        }
        
    });    
}


//Show All Active Products
module.exports.showActiveProducts = () => {

	return Product.find({isActive: true}).then(result => {

		return result
	})
};

module.exports.getAllProducts = (data) => {

    if (data.isAdmin) {
            return Product.find({}).then(result => {

            return result
        })
    } else {
        return false
    }
};


//RETRIEVE SPECIFIC PRODUCT
module.exports.getProduct = (reqParams) => {

    return Product.findById(reqParams.productId).then(result => {

        return result
    })

};


//UPDATE PRODUCT BY ADMIN ONLY
module.exports.updateProduct = (data, reqBody) => {

    if(data.isAdmin === true) {

            let updatedProduct = {
        img: reqBody.img,
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        stock: reqBody.stock
    }

            return Product.findByIdAndUpdate(data.productId, updatedProduct).then((product, error) => {
                if(error) {
                    return false
                } else {
                    return true
                }
            })

        } else {

            return false
        }

 }

 

 //ARCHIVE PRODUCT BY ADMIN ONLY
module.exports.archiveProduct = (data, reqBody) => {


        if(data.isAdmin === true) {

            let updateProductStatus ={
                isActive: reqBody.isActive
            }

            return Product.findByIdAndUpdate(data.productId, updateProductStatus).then((product, error) => {
                if(error) {
                    return false
                } else {
                    return true
                }
            })

        } else {

            return false
        }
}