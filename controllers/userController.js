const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Product = require('../models/Product');
const Order = require('../models/Order');


//Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if (result.length > 0) {
			return true

		} else {
			return false
		}

	})

}


//User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		phoneNo: reqBody.phoneNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10) 
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return false

		} else {
			return true
		}

	})
};

//User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if (result == null) {
			return false

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			if(isPasswordCorrect) {

				return { access: auth.createAccessToken(result)}

			} else {
				return false
			}
		}

	})
};

//Set user as admin by admin only
module.exports.updateUserStatus = (userData,reqBody) => {
	if (userData.isAdmin == true) {
		return User.findById(reqBody.userId).then(user => {
			user.isAdmin = reqBody.isAdmin
			return user.save().then((userIsAdmin, error) => {
				if(error){ return false }
					else { return true }
			})
		})
	} else {
		return "Not an Admin"
	}
}

module.exports.getAllUsers = (userData) => {

    if (userData.isAdmin) {
            return User.find({}).then(result => {

            return result
        })
    } else {
        return false
    }
};

// Retrieve User Details
module.exports.getProfile = (userData) => {

	return User.findById(userData.userId).then(result => {
		console.log(userData)
		if (result == null) {
			return false
		} else {
		
		return result;

		}
	});

};

/*// Retrieve Auth User's Orders
module.exports.getOrdersFromAuthUser = (userData) => {
	return User.find(userData.userId).then(user => {
		if (user.isAdmin == false) {
			return Order.find(userData.userId).then((orders, error) => {
				if(error){
					return false
				} else {
					return true
				}
			})
		} else {
			return false
		}
	})
	
}*/





