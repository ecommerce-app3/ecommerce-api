const Product = require('../models/Product')
const Order = require('../models/Order')

// ADD CUSTOMER ORDER
module.exports.addUserOrder = (reqBody, userData) => {
    if (userData.isAdmin == false) {

        
        return Product.findById(reqBody.productId).then(products => {
            console.log(products)
            let addUserOrder = new Order ({
            userId: userData.userId,
            products:{
               productId: reqBody.productId,
               quantity: reqBody.quantity, 
            },
            totalAmount: reqBody.quantity * products.price
        })
                if(products.isActive == true){
                    return addUserOrder.save().then((orderPlaced, error) => {
                        if(error){
                            return false
                        } else {
                            return true
                        }
                    })
                    //return products
                }
        })
    } else {
        return "Admins are not allowed to place order"
    }
}

module.exports.getAllOrders = (data) => {

    if (data.isAdmin) {
            return Order.find({}).then(result => {

            return result
        })
    } else {
        return false
    }
};

module.exports.statusOrder = (data, reqBody) => {


        if(data.isAdmin === true) {

            let updateOrderStatus ={
                status: reqBody.status
            }

            return Order.findByIdAndUpdate(data.orderId, updateOrderStatus).then((order, error) => {
                if(error) {
                    return false
                } else {
                    return true
                }
            })

        } else {

            return false
        }
}

module.exports.deleteOrder = (data) => {

        if(data.isAdmin === true) {

            return Order.deleteOne({_id: data.orderId}).then((order, error) => {
                console.log(data)
                console.log(data.orderId)
                console.log(order)
                if(error) {
                    return false
                } else {
                    return true
                }
            })

        } else {

            return false
        }
}
