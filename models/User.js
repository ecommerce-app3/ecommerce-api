const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: { type: String, required: [true, "This field is required"] },
	lastName: { type: String, required: [true, "This field is required"] },
	phoneNo: { type: String, required: [true, "This field is required"] },
	email : { type: String, required: [true, "This field is required"], unique: true },
	password: { type: String, required: [true, "This field is required"] },
	isAdmin: { type: Boolean, default: false },
})


module.exports = mongoose.model("User", userSchema);