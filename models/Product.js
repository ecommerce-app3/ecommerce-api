const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name : { type: String, required: [true, "This field is required"] },
	description: { type: String, required: [true, "This field is required"] },
	img: { type: String },
	price: { type: Number, required: [true, "This field is required"] },
	stock: { type: Number },
	isActive: { type: Boolean, default: true },
	createdOn: { type: Date, default: new Date() }
})

module.exports = mongoose.model("Product", productSchema);