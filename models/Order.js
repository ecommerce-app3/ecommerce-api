const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	userId : { type: String, required: [true, "This field is required"] },
	products: [{
		productId: { type: String },
		quantity: { type: Number, default: 1 }
	}],
	totalAmount: { type: Number },
	status: { type: String, default: "pending" },
	purchasedOn: { type: Date, default: new Date() }
})

module.exports = mongoose.model("Order", orderSchema);