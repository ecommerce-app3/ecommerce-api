const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require('../auth');

router.post('/', auth.verify, (req, res) => {
	let userData = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	orderController.addUserOrder(req.body,userData).then(resultFromController => res.send(resultFromController))
	
})

router.get("/all", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)


    orderController.getAllOrders(userData).then(resultFromController => res.send(resultFromController))
});

router.put("/:orderId/status", auth.verify, (req, res) => {

    const data = {
        orderId: req.params.orderId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    
    orderController.statusOrder(data, req.body).then(resultFromController => res.send(resultFromController))
});

router.delete("/:orderId/delete", auth.verify, (req, res) => {

    const data = {
        orderId: req.params.orderId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    
    orderController.deleteOrder(data).then(resultFromController => res.send(resultFromController))
});

module.exports = router;