const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');


// Route for Checking Existing Email
router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user registration
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user authentication
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user change as admin by admin only
router.put('/setAdmin', auth.verify, (req, res) => {

    const data = {
        userId : req.body.userId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    console.log(data);
    userController.updateUserStatus(data,req.body).then(resultFromController => res.send(resultFromController))
});


// Route for retrieving user details
// router.get("/:userId", (req, res) => {

//     userController.getUser(req.params).then(resultFromController => res.send(resultFromController))
// })

router.get("/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));

});

router.get("/all", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    userController.getAllUsers(userData).then(resultFromController => res.send(resultFromController));
})

/*// Route for retrieving authenticated user's orders
router.get("/order",auth.verify, (req, res) => {

    let userData = {
        userId: auth.decode(req.headers.authorization).id,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    userController.getOrdersFromAuthUser(userData).then(resultFromController => res.send(resultFromController))
})*/
 

module.exports = router;