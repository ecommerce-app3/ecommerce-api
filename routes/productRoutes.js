const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require("../auth");


// ADD PRODUCT
router.post("/addProduct", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    productController.addProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})

//RETRIEVE ACTIVE PRODUCTS
router.get("/", (req, res) => {

	productController.showActiveProducts().then(resultFromController => res.send(resultFromController))
});

router.get("/all", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)


    productController.getAllProducts(userData).then(resultFromController => res.send(resultFromController))
});


//RETRIEVE SPECIFIC PRODUCT
router.get("/:productId", (req, res) => {

    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});


//UPDATE PRODUCT BY ADMIN ONY
router.put('/:productId', auth.verify, (req, res) => {

  const data =  {
      productId: req.params.productId,
      isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  productController.updateProduct(data, req.body).then(resultFromController => res.send(resultFromController))
});


//ARCHIVE PRODUCT BY ADMIN ONLY
router.put("/:productId/archive", auth.verify, (req, res) => {

    const data = {
        productId: req.params.productId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    
    productController.archiveProduct(data, req.body).then(resultFromController => res.send(resultFromController))
});



module.exports = router;